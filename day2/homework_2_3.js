'use strict'

const peopleSalary2 = [{ "id":"1001","firstname":"Luke","lastname":"Skywalker","company":"Walt Disney","salary":"40000"},
{ "id":"1002","firstname":"Tony","lastname":"Stark","company":"Marvel","salary":"1000000"},
{ "id":"1003","firstname":"Somchai","lastname":"Jaidee","company":"Love2work","salary":"20000"},
{ "id":"1004","firstname":"Monkey D","lastname":"Luffee","company":"One Piece","salary":"9000000"}];

    for(let key of peopleSalary2){
        let salary = key['salary'];
        let newSalary = [];
        for(let i= 0; i < 3 ; i++){
            salary = parseInt(salary) + (parseInt(salary) * 0.1)
            newSalary.push(salary);
        }       
        key['salary'] = newSalary;
        delete key['company']; 
    }

    for(let i in peopleSalary2){
        let obj = peopleSalary2[i];
        //Header
        if(i == 0){
            for(let key in obj){
                $('#tb2').append("<th>"+ key+"</th>");
            }
        } 
        //Detail
        $('#tb2').append("<tr>");
            for(let key in obj){     
                //console.log(key);
                if(key == "salary"){
                    let detail = ""; 
                    for(let item of obj[key]){
                          detail += "<li>"+ item +"</li>";
                    }
                    $('#tb2').append("<td><ol>"+ detail +"</ol></td>");
                }else{    
                    $('#tb2').append("<td>"+ obj[key]+"</td>");
                }               
            }
         $('#tb2').append("</tr>");  
    }