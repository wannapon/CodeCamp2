fetch('homework1-4.json').then(response => {
    return response.json();
}).then(myJson => {
    let people = myJson;
    let newPeople = [];
    newPeople = people
        .filter(person => person['friends'].length >= 2)
        .map(person => {
            let { name, gender, company, email, friends, balance } = person;
            person = { name, gender, company, email, friends, balance };
            person['balance'] = "$" + (parseFloat(person['balance'].replace(/[$,]/g, '')) / 10).toFixed(2);
            let newPerson = { ...person };
            return newPerson;
        });

    console.log(newPeople);

    drawTable(newPeople);

}).catch(error => {
    console.error('Error:', error);
});

function drawTable(people) {
    for (let i in people) {
        let obj = people[i];
        //Header
        if (i == 0) {
            $('#thead').append(`<th>No.</th>`);
            for (let key in obj) {
                $('#thead').append(`<th>${key}</th>`);
            }
        }

        //Detail
        $('#tb').append("<tr>");
        $('#tb').append(`<td>${parseInt(i) + 1}</td>`);
        for (let key in obj) {

            if (key == 'friends') {
                let result = "";
                for (let i in obj[key]) {
                    result += `<li>${obj[key][i]['name']}</li>`;
                }
                $('#tb').append(`<td><ul>${result}</ul></td>`);
            } else {
                $('#tb').append(`<td>${obj[key]}</td>`);
            }

        }
        $('#tb').append("</tr>");
    }
}
