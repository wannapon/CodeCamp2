fetch('homework1.json').then(response => {
    return response.json();
}).then(myJson => {
    let peopleSalary = myJson;
    peopleSalary = peopleSalary.map(person => {
        //person['salary'] = parseInt(person['salary']);
        person.group = parseInt(person.salary) > 20000 ? 'Group1' : 'Group2';
        return person;
    });
    let sortedSalary = _.sortBy(peopleSalary, [o => parseInt(o.salary)]);
    console.log(sortedSalary);

    //let groupedSalary = _.groupBy(sortedSalary, [o => parseInt(o.salary) > 20000]);
    let groupedSalary = _.groupBy(sortedSalary, s => s.group);
    console.log(groupedSalary);
    // let peopleLowSalary = [];
    // peopleLowSalary = peopleSalary.filter(person => parseInt(person['salary']) < 100000).map(person => {
    //     person['salary'] = parseInt(person['salary']) * 2;
    //     return person;
    // });

    // let sumSalary = peopleSalary.reduce((sum, person) => sum + parseInt(person['salary']), 0);

    // console.log(peopleLowSalary);
    // console.log(sumSalary);

    // drawTable(peopleLowSalary);
    // $('#tb').append(`<tr><td colspan="4" align="right"><b>Sum Salary</b></td><td align="right">${sumSalary}</td></tr>`);

}).catch(error => {
    console.error('Error:', error);
});

function drawTable(people) {
    for (let i in people) {
        let obj = people[i];
        //Header
        if (i == 0) {
            //$('#tb').append(`<thead class="thead-dark">`);
            for (let key in obj) {
                $('#thead').append(`<th>${key}</th>`);
            }
            //$('#tb').append(`</thead>`);
        }

        //Detail
        $('#tb').append("<tr>");
        for (let key in obj) {
            $('#tb').append(`<td align="right">${obj[key]}</td>`);
        }
        $('#tb').append("</tr>");
    }
}
