'use strict'
$(document).ready(function(){
    let input1 = [1,2,[5,6,[8,9]]];
    let input2 =  {a:1,b:2,c:3,d:4};
    let input3 =  [1,2,{a:1,b:2}];
    let input4 =  [1,2,{a:1,b:{c:3,d:4}}];

    let arr1 = input3;//[1,2,3,4,[55,[{r:1,b:2},4,5]],6,7];  
    let arr2 = cloneArray(arr1);
    //arr2.a = 555;
    console.log('-- [ arr1 ] --');
    console.log(arr1);
    console.log('-- [ new arr ] --');
    console.log(arr2);
});

function cloneArray(arr){
    //let newItem;   
    if(Array.isArray(arr)){
        let outterCount= arr.length;
        let newArr = [];
        for(let i = 0 ; i < outterCount; i++){
            let innerCount = arr[i].length;
            if(innerCount > 1){
                newArr[i]= cloneArray(arr[i]);
            }else{
                newArr[i] = arr[i];
            }
            return newArr;
        }      
    } else{
        let outterCount = Object.keys(arr).length;
        let newObj = [];
        if(outterCount > 0){
            for(let i in arr){
                let innerCount = 0;
                if(Array.isArray(arr)){
                    innerCount = arr[i].length;
                }else{
                    innerCount = Object.keys(arr[i]).length;
                    if(innerCount > 1){
                        newObj[i] = cloneArray(arr[i]);
                    }else {
                        newObj[i] = arr[i];
                    }       
                }       
            }    
            return newObj;  
        }
    }
}


