'use strict'

function addAdditionalFields(employees){
    let temp = [];
    for(let i in employees){
        temp = [...temp,addYearSalary(employees[i])];
    }
    employees = temp;

    let temp2 = [];
    for(let i in employees){
        temp2 = [...temp2,addNextSalary(employees[i])];
    }
    
    return employees = temp2;
}

function addYearSalary(person) {
    return person = {...person, yearSalary : person['salary'] * 12 } ;  
}

function addNextSalary(person){
    let salary = person['salary'];
    let newSalary = [];
    
    for(let i= 0; i < 3 ; i++){
        salary = parseInt(salary) + (parseInt(salary) * 0.1);
        newSalary.push(salary);
    }     
    return person = {...person, nextSalary : newSalary } ;
}

function drawTable(employees){
    $('#tb').append("<table >");
    for(let i in employees){
        let obj = employees[i];
        //Header
        if(i == 0){
            for(let key in obj){
                $('#tb').append("<th>"+ key+"</th>");
            }
        } 
        //Detail
        $('#tb').append("<tr>");
            for(let key in obj){     
                if(key == "nextSalary"){
                    let detail = ""; 
                    for(let item of obj[key]){
                          detail += "<li>"+ item +"</li>";
                    }
                    $('#tb').append("<td><ol>"+ detail +"</ol></td>");
                }else{    
                    $('#tb').append("<td>"+ obj[key]+"</td>");
                }               
            }
         $('#tb').append("</tr>");  
    }
    $('#tb').append("</table><br/>");
}

fetch('homework2_1.json').then(response => {
    return response.json();
})
.then(myJson => {
    let employees = myJson;
    let newEmployees = addAdditionalFields(employees);
    newEmployees[1].salary = 0;

    console.log('------ employees ------');
    console.log(employees);
    console.log('------ newEmployees ------');
    console.log(newEmployees);
    
    drawTable(employees);
    drawTable(newEmployees);
 })
 .catch(error => {
    console.error('Error:', error);
 });

