'use strict'

let arr1 = [1,2,{a:1,b:{c:3,d:4}}];
//let arr1 = [1,2,3,4,[55,[3,4,5]],6,7];  
let arr2 = cloneArray(arr1);
arr2[2].a = 'AA';
console.log('-- [ arr1 ] --');
console.log(arr1);
console.log('-- [ new arr ] --');
console.log(arr2);


function cloneArray(arr){
    let newArr = Array.isArray(arr) ? [] : {} ; 
        
    for (let i in arr){ 
        if(Object.keys(arr[i]).length > 0 || arr[i].length > 0){ 
                newArr[i]= cloneArray(arr[i]);
        }else{
                newArr[i] = arr[i];
        }
    }  
        return newArr;

}
