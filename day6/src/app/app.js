import '../styles/styles.css';
import $ from 'jquery';
import 'popper.js';
import 'bootstrap';
import dt from 'datatables.net-bs4';
import {greeting} from './feature1/feature';
import bs from 'bootstrap-select';

$(function () {
    greeting('student');
    $('table').dataTable();
    $('.selectpicker').selectpicker({size: 4});
    $('#ddlTheme').change(function () {
        let theme = $('#ddlTheme').val();
        $('body').removeClass('pastel');
        $('body').removeClass('dark');
        $('body').addClass(theme);
        if (theme == 'dark') {
            $('.table').addClass('table-light');
            $('thead').removeClass('thead-pastel');
            $('thead').addClass('thead-dark');
        } else if (theme == 'pastel') {
            $('.table').addClass('table-light');
            $('thead').removeClass('thead-dark');
            $('thead').addClass('thead-pastel');
        } else {
            $('.table').removeClass('table-light');
            $('thead').removeClass();
        }
    });
});